package crypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"io"
)

func DecryptWithPass(encryptedText string, password string) (string, error) {
	cipherText, err := base64.StdEncoding.DecodeString(encryptedText)
	if err != nil {
		return "", err
	}

	//	Create key from password
	cipherBlockLength := 32
	cipherKey := make([]byte, cipherBlockLength)
	copy(cipherKey, password)

	//	Get initialize vector
	iv := cipherText[:aes.BlockSize]
	//fmt.Println("iv:", base64.StdEncoding.EncodeToString(iv))

	//	Get hmac hash
	hmacHash := cipherText[aes.BlockSize : aes.BlockSize+cipherBlockLength]

	//  Get clean encrypted text
	cipherText = cipherText[aes.BlockSize+cipherBlockLength:]

	//	Compare hmac hash
	hash := hmac.New(sha256.New, cipherKey)
	hash.Write(cipherText)
	if !hmac.Equal(hmacHash, hash.Sum(nil)) {
		return "", errors.New("hmac hash not equal")
	}

	//	Decode encoded text, never error if cipherBlockLength = 32
	cipherBlock, _ := aes.NewCipher(cipherKey)

	cipher.NewCBCDecrypter(cipherBlock, iv).CryptBlocks(cipherText, cipherText)

	//	Unpad can't error after correct decrypt
	cipherText, _ = Unpad(cipherText, aes.BlockSize)

	return string(cipherText), nil
}

func EncryptWithPass(encryptedText string, password string) (string, error) {
	//	Create key from password
	cipherBlockLength := 32
	cipherKey := make([]byte, cipherBlockLength)
	copy(cipherKey, password)

	plainText := Pad([]byte(encryptedText), aes.BlockSize)

	//	Create encoder, never error if cipherBlockLength = 32
	cipherBlock, _ := aes.NewCipher(cipherKey)

	cipherText := make([]byte, aes.BlockSize+cipherBlockLength+len(plainText))
	iv := cipherText[:aes.BlockSize]
	_, _ = io.ReadFull(rand.Reader, iv)

	mode := cipher.NewCBCEncrypter(cipherBlock, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize+cipherBlockLength:], plainText)

	//	Compare hmac hash
	hash := hmac.New(sha256.New, cipherKey)
	hash.Write(cipherText[aes.BlockSize+cipherBlockLength:])
	hmacHash := cipherText[aes.BlockSize:aes.BlockSize+cipherBlockLength]
	copy(hmacHash, hash.Sum(nil))

	return base64.StdEncoding.EncodeToString(cipherText), nil
}

func Pad(buf []byte, size int) []byte {
	bufLen := len(buf)
	padLen := size - bufLen%size
	padded := make([]byte, bufLen+padLen)
	copy(padded, buf)
	for i := 0; i < padLen; i++ {
		padded[bufLen+i] = byte(padLen)
	}
	return padded
}

func Unpad(padded []byte, size int) ([]byte, error) {
	if len(padded)%size != 0 {
		return nil, errors.New("padded value wasn't in correct size")
	}

	bufLen := len(padded) - int(padded[len(padded)-1])
	buf := make([]byte, bufLen)
	copy(buf, padded[:bufLen])
	return buf, nil
}
